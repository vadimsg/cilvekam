<div class="c-edit-content">
    <div class="c-edit-bttn"><span></span></div>
    <div class="c-edit-box">
        <a data-fancybox="c-edit" data-src="#c-edit-{{ $id }}" href="javascript:;">Labot</a>
        <a href="javascript:;" data-comment-id="{{ $comment->id }}" class="delete-comment">Dzēst</a>
    </div>
    <div class="c-edit" id="c-edit-{{ $id }}">
    <div>
    <div class="comment-update-form">
        <h4>Labot ierakstu<span data-fancybox-close></span></h4>
        <form class="c-edit-form">
            <div class="c-edit-text">
                {{-- <div class="c-img"></div> --}}
                <textarea>{!! nl2br($comment->body) !!}</textarea>
                <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
                </div>
            </div>
            <ul>
            <li @if(!$comment->file) style="display:none" @endif><div style="background:url({{ asset('storage/comments/'.$comment->file) }})"></div><span></span></li>
            <input  @if($comment->file) style="display:none" @endif type="file" accept="image/*" />
            {{-- <li><div><input type="file" name="myfile" /></div></li>
            <li><div><input type="file" name="myfile" /></div></li>
            <li><div><input type="file" name="myfile" /></div></li> --}}
            </ul>
            <button data-comment-id="{{ $id }}"  data-commentable-id="{{ $commentableId }}" data-commentable-type="{{ $commentableType }}" type="button" class="update-comment">Saglabāt</button>
        </form>
    </div>
    </div>
    </div>
</div>
