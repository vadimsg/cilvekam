<div class="cookies-container">
    <div>
        <div class="close-cookies"></div>
        <div class="cookies">Šī vietne izmanto sīkdatnes, lai uzlabotu lietošanas pieredzi un optimizētu tās darbību. Turpinot lietot šo vietni,
            jūs piekrītat <a data-fancybox data-src="#cookies" href="javascript:;">sīkdatņu lietošanas noteikumiem</a>!
        </div>
    </div>
</div>
