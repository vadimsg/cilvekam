<div class="header">
    <a href="#" class="logo-mob"></a>
    <div id="menu-bttn">
        <div class="m-line-1"></div>
        <div class="m-line-2"></div>
        <div class="m-line-3"></div>
    </div>
    <div class="find">
        <div>
            <a href="/" class="logo"></a>
            @if(auth()->user()->game)
                <a href="{{ route('tasks') }}">
                    {{ auth()->user()->game->name }}
                </a>
            @endif
        </div>
    </div>
    <div class="top-content">
        <div class="character">
            <div>
                <div class="character-icon ch-{{ auth()->user()->rank }}-h"></div>
                <span>Tu jau esi</span>
                <div class="character-title">{{ auth()->user()->rank_name }}</div>
                <div class="ul-slide-bttn"></div>
            </div>
        </div>
        @if(auth()->user()->game)
            <div class="top-blocks">
                <ul>
                    <li class="your-points">
                        <div>
                            <span class="block-title">Tavi punkti</span>
                            <span class="points"><strong>{{ auth()->user()->points }}</strong> <span>no {{ auth()->user()->game->max_points }}</span></span>
                            <div class="progress-bar">
                                <div style="width: {{ auth()->user()->points_progress }}%"><span>{{ auth()->user()->points_progress }}%</span></div>
                            </div>
                        </div>
                    </li>
                    @if(auth()->user()->game && !auth()->user()->game->team_points_is_hidden)
                        <li class="team-points">
                            <div>
                                <span class="block-title">Komandas punkti</span>
                                <span class="points"><strong>{{ auth()->user()->team_points }}</strong> <span>no {{ auth()->user()->game->max_points_team }}</span></span>
                                <div class="progress-bar">
                                    <div style="width: {{ auth()->user()->team_points_progress }}%"><span>{{ auth()->user()->team_points_progress }}%</span></div>
                                </div>
                            </div>
                        </li>
                    @endif
                    @if(auth()->user()->game && !auth()->user()->game->donation_is_hidden)
                        <li class="donate">
                            <div>
                                <span class="block-title">Palīdzam kopā</span>
                                {{-- <span class="block-text">foršai lauku skolai Latgalē nepieciešami <strong>0 eiro</strong></span> --}}
                                <span class="block-text">Ja savācam kopā 10 000 punktus, veltam @if(auth()->user()->game->id === 9) 150 @else 100 @endif  Eiro...</span>
                                <div class="progress-bar">
                                    <div style="width: {{ auth()->user()->game->getDonationProgress() }}%"><span>{{ auth()->user()->game->getUserTotalPoints() }} punkti</span></div>
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        @endif
        <!-- top-blocks -->
    </div>
    <!-- top-content -->
</div>
<!-- header -->
