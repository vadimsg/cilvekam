<ul>
    @foreach($comments as $comment)
        <li>
            @include('partials.comment', ['commentableId' => $commentableId, 'commentableType' => $commentableType, 'comment' => $comment])
            <ul>
                @foreach($comment->children as $children)
                    <li>
                        @include('partials.comment', ['commentableId' => $commentableId, 'commentableType' => $commentableType, 'comment' => $children])
                    </li>
                @endforeach
            </ul>
        </li>
    @endforeach
</ul>
