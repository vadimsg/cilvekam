<div class="c-content @if($comment->user->admin) -c-highlighted @endif">
    <div class="c-img-big" id="c-img-big-{{ $comment->id }}">
        <div>
             @if($comment->user->picture)
                <img src="{{ asset('storage/avatars/'.$comment->user->picture) }}">
            @else
                <img src="/img/user.png">
            @endif
            <a href="{{ route('user', $comment->user->id) }}">Uzraksti komentāru</a>
        </div>
    </div>
    <a data-fancybox data-src="#c-img-big-{{ $comment->id }}" href="javascript:;" class="c-img" @if($comment->user->picture) style="background-image:url({{ asset('storage/avatars/'.$comment->user->picture) }})" @endif></a>
    <div class="c-top">
        <a href="{{ route('user', $comment->user->id) }}">{{ $comment->user->firstname }} {{ $comment->user->lastname }}</a>
        <span>{{ $comment->created_at->format('d.m.Y H.i') }}</span>
    </div>
    <div class="c-text">
        <div class="c-text-container">
            <span class="c-text-body">{!! nl2br($comment->body) !!}</span>
            {{-- <span class="c-text-body">{!! nl2br(e($comment->body)) !!}</span> --}}
            @if($comment->file)
                <div class="c-text-img">
                    <a data-fancybox href="{{ asset('storage/comments/'.$comment->file) }}" data-width="800">
                        <img src="{{ asset('storage/comments/'.$comment->file) }}">
                    </a>
                </div>
            @endif
        </div>
        @if(auth()->user()->id === $comment->user_id)
            @include('partials.comment_edit', ['id' => $comment->id, 'commentable_id' => $comment->commentable_id, 'commentable_type' =>  'task', 'comment' => $comment])
        @endif
        @if(!$comment->parent_id)
            <div class="c-reply">Atbildēt</div>
        @endif
        <a href="javascript:;" class="morelink">Vairāk</a>
    </div>
    <div class="add-comment">
        <form>
            <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
            <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
            </div>
            <div class="add-image"><input type="file" accept="image/*" /></div>
            <button data-parent-id="{{ $comment->id }}" data-commentable-id="{{ $commentableId }}" data-commentable-type="{{ $commentableType }}" type="button" class="add"></button>
        </form>
    </div>
</div>
