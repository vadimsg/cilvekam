<div class="menu">
    <div class="profile-block">
        <div class="profile-icon" @if(auth()->user()->picture) style="background-image: url({{ asset('storage/avatars/'.auth()->user()->picture) }})" @endif></div>
        {{-- <div class="profile-icon"><a href="mans_profils.html#comments">4</a></div> --}}
        <div class="name">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
        <a href="{{ route('logout') }}" class="logout">Iziet</a>
    </div>
    <!-- profile-block -->
    <div class="nav">
        <ul>
            <li><a href="/" class="{{ Route::currentRouteName() === 'home' ? 'current' : '' }}">Mans profils</a></li>
            <li><a href="/tasks" class="{{ in_array(Route::currentRouteName(), ['tasks', 'task']) ? 'current' : '' }}">Spēles uzdevumi</a></li>
            <li><a href="/feed" class="{{ Route::currentRouteName() === 'feed' ? 'current' : '' }}">Čalotava</a></li>
            <li><a href="/results" class="{{ Route::currentRouteName() === 'results' ? 'current' : '' }}">Rezultātu tabula</a></li>
            <li><a href="/methods" class="{{ Route::currentRouteName() === 'methods' ? 'current' : '' }}">Metodes</a></li>
            <li><a href="/faq" class="{{ Route::currentRouteName() === 'faq' ? 'current' : '' }}">FAQ</a></li>
            <li><a href="/rules" class="{{ Route::currentRouteName() === 'rules' ? 'current' : '' }}">Spēles noteikumi</a></li>
            @if(auth()->user()->game && !auth()->user()->game->donation_is_hidden)
                <li><a href="/help-together" class="{{ Route::currentRouteName() === 'help-together' ? 'current' : '' }}">Palīdzam kopā</a></li>
            @endif
            <li><a href="http://cilvekam.lv/" target="_blank" class="cilvekam">Cilvēkam.lv</a></li>
        </ul>
    </div>
</div>
<!-- menu -->
