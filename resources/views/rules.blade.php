@extends('layouts.main')
@section('title', 'Noteikumi')
@section('content')
<h1>Spēles noteikumi</h1>
<div class="wide-bg">
    @if($page)
        {!! $page->body !!}
    @endif
</div>
<!-- wide-bg -->
@endsection
