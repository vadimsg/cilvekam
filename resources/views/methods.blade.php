@extends('layouts.main')
@section('title', 'Metodes')
@section('content')
<h1>Metodes</h1>
<div class="wide-bg grey-bg">
    <div class="methods">
        <ul>
            @unless ($tasks->count())
                <li>Neviena metode netika atrasta.</li>
            @endunless
            @foreach($tasks as $task)
                <li><span class="day">{{ $task->day }}<span>diena</span></span>
                    <div>{{ $task->name }} <span>{{ $task->human_type }}</span><a href="{{ route('method', $task->method) }}">Metode</a></div>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- methods -->
</div>
<!-- wide-bg -->
@endsection
