@extends('layouts.main')
@section('title', 'Spēles uzdevumi')
@section('content')
<div class="wide-bg">
    @if($game)
        <h1>{{ $game->name }}</h1>
    @else
        <h1>Nav izvēlēta neviena spēle</h1>
    @endif

    @if($game)
        <div class="tasks-text">
            {{ $game->description }}
        </div>
    @endif

    <!-- find-text -->

    <div class="tasks-content">
        <div class="tasks">
            @foreach($tasks as $task)
                <div>
                    <a href="{{ route('task', $task->id) }}" class="{{ auth()->user()->getTaskStatus($task) }}">
                        {{ $task->name }}
                        <div>{{ $task->human_type }}</div>
                        <span>
                            @if($task->card_label)
                                {{ $task->card_label }}
                            @else
                                {{ $task->day }}. diena
                            @endif
                        </span>
                    </a>
                </div>
            @endforeach
        </div>
        <!-- tasks -->
    </div>
    <!-- tasks-content -->

    @if($game)
        <div class="task-m">
            <div class="task-r">Uzdevums nav izpildīts</div>
            <div class="task-o">Uzdevums tiek izskatīts</div>
            <div class="task-g">Uzdevums pieņemts</div>
        </div>
    @endif
</div>
<!-- wide-bg -->
@endsection
