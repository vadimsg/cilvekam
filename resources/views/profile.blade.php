@extends('layouts.main')
@section('title', 'Mans profils')
@section('content')
<div class="my-profile">
    <h3>Mans profils</h3>
    <div>
        <form action="{{ route('update-profile') }}" enctype="multipart/form-data" method="post" validate="true">
            @csrf
            <div class="my-profile-img">
                <div class="profile-big-icon" @if(auth()->user()->picture) style="background-image:url({{ asset('storage/avatars/'.auth()->user()->picture) }})" @endif>
                    <div class="edit"><input type="file" name="picture" accept="image/*"></div>
                </div>
                {{-- <input id="facebook" name="facebook" type="checkbox" /> --}}
                {{-- <label for="facebook">Parādīt manu Facebook</label> --}}
            </div>

            <div class="my-profile-descr">
                <div class="input-block">
                    <input type="text" name="firstname" placeholder="Vārds" value="{{ old('firstname') ?? auth()->user()->firstname }}" required>
                    <input type="text" name="lastname" placeholder="Uzvārds" value="{{ old('lastname') ?? auth()->user()->lastname }}" required>
                </div>
                <div class="input-block" style="float: right">
                    <input type="text" name="phone" value="{{ old('phone') ?? auth()->user()->phone }}" placeholder="Telefons">
                    {{-- <input type="text" name="email" value="{{ old('email') }}" placeholder="E-pasts" disabled> --}}
                </div>

                {{-- <div class="input-title">Mainīt paroli</div>
                <div class="input-block">
                    <input type="password" value="{{ old('password') }}" name="password" placeholder="Parole">
                </div>
                <div class="input-block" style="float: right">
                    <input type="password" name="password_confirmation" placeholder="Parole atkārtoti">
                </div> --}}
                <div class="input-title"> Obligāti aizpildāmie lauciņi</div>
                <input type="text" name="city" value="{{ old('city') ?? auth()->user()->city }}" placeholder="Valsts, pilsēta..." required="required">
                <textarea maxlength="150" name="interests" placeholder="Manas intereses dzīvē..." required="required">{{ old('interests') ?? auth()->user()->interests }}</textarea>
                <textarea maxlength="150" name="motivation" placeholder="Ko vēlos attīstīt, uzzināt spēlē..." required="required">{{ old('motivation') ?? auth()->user()->motivation }}</textarea>

                <div class="input-about">
                    <div class="input-title"><strong>Vairāk par mani</strong><span>Maks. 300 zīmes</span></div>
                    <textarea name="about" maxlength="300">{{ old('about') ?? auth()->user()->about }}</textarea>
                </div>
                <!-- input-about -->
            </div>
            <!-- my-profile-descr -->
            <button type="submit">Saglabāt</button>
        </form>
    </div>
    <!-- my-profile-div -->
</div>
<!-- my-profile -->

<div class="available-games">
    <h3>Pieejamās spēles</h3>

    <div class="games">
        <ul>
            @foreach($games as $game)
            <li>
                <a href="{{ route('tasks', ['game_id' => $game->id]) }}">
                        {{ $game->name }}
                        <div class="game-arr"></div>
                        @if(auth()->user()->game_id === $game->id)
                            <span></span>
                        @endif
                    </a>
            </li>
            @endforeach
        </ul>
    </div>
    <!-- games -->
</div>
<!-- available-games -->

<div class="comments" id="comments-block">
    <h3>Komentāri</h3>

    <div class="add-comment">
        <form>
            <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
            <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
            </div>
            <div class="add-image"><input type="file" name="image"></div>
            <button data-commentable-type="user" data-commentable-id="{{ auth()->user()->id }}" class="add" type="button" disabled="disabled"></button>
        </form>
    </div>

    <div class="comments-list">
    @include('partials.comments', ['commentableId' => auth()->user()->id, 'commentableType' => 'user', 'comments' => App\Comment::getCommentsWithReplies($user)])
    </div>
    <!-- comments-list -->
</div>
<!-- comments -->
@endsection
