@extends('layouts.main')
@section('title', 'Palīdzam kopā')
@section('content')
<div class="title" style="background-image:url(/img/palidzam_kopa.jpg)">
    <div>Palīdzam kopā</div>
</div>

@foreach($donations as $donation)
    <div class="text comments">

        <div class="text-content">
            <div class="c-img-big" id="c-img-big-{{ $donation->user->id }}">
                <div>
                    @if($donation->user->picture)
                        <img src="{{ asset('storage/avatars/'.$donation->user->picture) }}">
                    @else
                        <img src="/img/user.png">
                    @endif
                    <a href="{{ route('user', $donation->user->id) }}">Uzraksti komentāru</a>
                </div>
            </div>
            <a data-fancybox data-src="#c-img-big-{{ $donation->user->id }}" href="javascript:;" class="c-img" @if($donation->user->picture) style="background-image:url({{ asset('storage/avatars/'.$donation->user->picture) }})" @endif></a>
            <div class="c-top">
                <a href="publiskais_profils.html">{{ $donation->user->firstname }} {{ $donation->user->lastname }}</a>
                <span>{{ $donation->created_at->format('d.m.Y H.i') }}</span>
            </div>

            <div class="descr">
               {!! $donation->body !!}
            </div>

            {{-- <div class="bttns">
                <div class="like">Patīk</div>
                <div class="like-box">
                    <span>
                        <div>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>un 5 citi...</span>
                        </div>
                        15 dalībniekiem patīk
                    </span>
                </div>
                <div class="c-bttn">5 komentāri</div>
            </div> --}}
        </div>
    <!-- text-content -->

    <div class="add-comment">
        <form>
            <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
            <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
            </div>
            <div class="add-image"><input type="file" name="image"></div>
            <button data-commentable-type="feed" data-commentable-id="{{ $donation->id }}" class="add" type="button" disabled="disabled"></button>
        </form>
    </div>

    <div class="comments-list">
        @include('partials.comments', ['commentableId' => $donation->id, 'commentableType' => 'feed', 'comments' => App\Comment::getCommentsWithReplies($donation)])
    </div>
    <!-- comments-list -->

    </div>
    <!-- text comments -->
@endforeach
@endsection
