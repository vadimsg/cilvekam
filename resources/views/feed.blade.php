@extends('layouts.main')
@section('title', 'Čalotava')
@section('content')
<div class="title" style="background-image:url(img/calotava.jpg)">
    <div>Čalotava</div>
</div>

@foreach($feeds as $feed)
    <div class="text comments">

        <div class="text-content">
            <div class="c-img-big" id="c-img-big-{{ $feed->user->id }}">
                <div>
                    @if($feed->user->picture)
                        <img src="{{ asset('storage/avatars/'.$feed->user->picture) }}">
                    @else
                        <img src="/img/user.png">
                    @endif
                    <a href="{{ route('user', $feed->user->id) }}">Uzraksti komentāru</a>
                </div>
            </div>
            <a data-fancybox data-src="#c-img-big-{{ $feed->user->id }}" href="javascript:;" class="c-img" @if($feed->user->picture) style="background-image:url({{ asset('storage/avatars/'.$feed->user->picture) }})" @endif></a>
            <div class="c-top">
                <a href="{{ route('user', $feed->user->id) }}">{{ $feed->user->firstname }} {{ $feed->user->lastname }}</a>
                <span>{{ $feed->created_at->format('d.m.Y H.i') }}</span>
            </div>

            <div class="descr">
               {!! $feed->body !!}
            </div>

            {{-- <div class="bttns">
                <div class="like">Patīk</div>
                <div class="like-box">
                    <span>
                        <div>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>Vārds uzvārds</span>
                            <span>un 5 citi...</span>
                        </div>
                        15 dalībniekiem patīk
                    </span>
                </div>
                <div class="c-bttn">5 komentāri</div>
            </div> --}}
        </div>
    <!-- text-content -->

    <div class="add-comment">
        <form>
            <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
            <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
            </div>
            <div class="add-image"><input type="file" name="image"></div>
            <button data-commentable-type="feed" data-commentable-id="{{ $feed->id }}" class="add" type="button" disabled="disabled"></button>
        </form>
    </div>

    <div class="comments-list">
        @include('partials.comments', ['commentableId' => $feed->id, 'commentableType' => 'feed', 'comments' => App\Comment::getCommentsWithReplies($feed)])
    </div>
    <!-- comments-list -->

    </div>
    <!-- text comments -->
@endforeach
@endsection
