@extends('layouts.auth')
@section('title', 'Auth')
@section('content')

<div class="enter">
    <ul>
        <li><a href="/login">Pieslēgties</a></li>
        <li><a href="/register">Reģistrēties</a></li>
    </ul>
    <div id="login">
        @if (session('status'))
            <h5>
                {{ session('status') }}
            </h5>
            <br>
        @endif
        <form method="POST" action="{{ route('password.email') }}" novalidate>
            @csrf
            <input type="text" name="email" value="{{ old('email') }}" placeholder="E-pasts" required>
            @if ($errors->has('email'))
                <strong class="form-error">{{ $errors->first('email') }}</strong>
            @endif
            <button type="submit">Sūtīt paroles maiņas saiti</button>
        </form>
    </div>
</div>

@endsection
