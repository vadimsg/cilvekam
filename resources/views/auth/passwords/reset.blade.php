@extends('layouts.auth')
@section('title', 'Auth')
@section('content')

<div class="enter">
    <ul>
        <li><a href="/login">Pieslēgties</a></li>
        <li><a href="/register">Reģistrēties</a></li>
    </ul>
    <div id="login">
        @if (session('status'))
            <h5>
                {{ session('status') }}
            </h5>
            <br>
        @endif
        <form method="POST" action="{{ route('password.update') }}" novalidate>
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="text" name="email" value="{{ old('email') }}" placeholder="E-pasts" required>
            @if ($errors->has('email'))
                <strong class="form-error">{{ $errors->first('email') }}</strong>
            @endif
            <input type="password" name="password" value="{{ old('password') }}" placeholder="Parole" required>
            @if ($errors->has('password'))
                <strong class="form-error">{{ $errors->first('password') }}</strong>
            @endif
            <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Atkārtot paroli" required>
            @if ($errors->has('password_confirmation'))
                <strong class="form-error">{{ $errors->first('password_confirmation') }}</strong>
            @endif
            <button type="submit">Mainīt paroli</button>
        </form>
    </div>
</div>

@endsection
