@extends('layouts.auth')
@section('title', 'Auth')
@section('content') @if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
{{-- <div class="intro-title">Spēle
    @if($game)
    <h1>{{ $game->name }}</h1>
    @endif
</div> --}}
<div class="enter">
    <ul>
        <li><a href="/login">Pieslēgties</a></li>
        <li class="ui-tabs-active"><a href="/register">Reģistrēties</a></li>
    </ul>
    <div id="register" style="width:100%;">
        {{-- @if($email && !$error) --}}
            <form method="POST" action="{{ route('register') }}" novalidate>
                @csrf
                <input type="hidden" name="invite_token" value="{{ request()->get('token') }}">
                <input type="text" name="firstname" value="{{ old('firstname') }}" placeholder="Vārds" required>
                @if ($errors->has('firstname'))
                    <strong class="form-error">{{ $errors->first('firstname') }}</strong>
                @endif
                <input type="text" name="lastname" value="{{ old('lastname') }}" placeholder="Uzvārds" required>
                @if ($errors->has('lastname'))
                    <strong class="form-error">{{ $errors->first('lastname') }}</strong>
                @endif
                <input type="text" name="email" value="{{ old('email', $email) }}" placeholder="E-pasts" required>
                @if ($errors->has('email'))
                    <strong class="form-error">{{ $errors->first('email') }}</strong>
                @endif
                <input type="password" name="password" value="{{ old('password') }}" placeholder="Parole" required>
                @if ($errors->has('password'))
                    <strong class="form-error">{{ $errors->first('password') }}</strong>
                @endif
                <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Atkārtot paroli" required>
                @if ($errors->has('password_confirmation'))
                    <strong class="form-error">{{ $errors->first('password_confirmation') }}</strong>
                @endif
                <button type="submit">Reģistrēties</button>
                <input id="terms" name="terms" type="checkbox" value="yes" required="required" {{ old( 'terms') ? 'checked' : '' }} />
                <label for="terms">Piekrītu <a data-fancybox data-src="#rules" href="javascript:;">spēles lietošanas noteikumiem</a></label>
                @if ($errors->has('terms'))
                    <strong class="form-error">{{ $errors->first('terms') }}</strong>
                @endif
                {{-- <div class="reg-facebook">Reģistrēties ar <a href="https://www.facebook.com/" target="_blank">Facebook</a></div> --}}
            </form>
        {{-- @endif

        @if(!$email && !$error)
            <p>E-pasts ar šādu ielūgumu netika atrasts.</p>
        @endif

        @if($error)
            <p>Reģistrācija šobrīd iespējama tikai caur ielūgumiem.</p>
        @endif --}}
    </div>
</div>
<!-- enter -->

<div class="forgot-content popup" id="forgot">
    <div>
        <h2>Atjaunot paroli</h2>
        Ievadiet savu e-pasta adresi, un mēs Jums nosūtīsim paroles atjaunošanas saiti.
        <form action="" method="post" validate="true">
            <input type="text" placeholder="E-pasts" required="required">
            <button type="submit">Nosūtīt</button>
        </form>
    </div>
</div>
<!-- forgot-content -->

<div class="rules-content popup" id="rules">
    <div>
        <h5>Spēles lietošanas noteikumi</h5><br />
        <br />
        @if($rules)
            {!! $rules->body !!}
        @endif
    </div>
</div>
<!-- rules-content -->
@endsection
