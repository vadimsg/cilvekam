@extends('layouts.auth')
@section('title', 'Auth')
@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
<div class="enter">
    <ul>
        <li class="ui-tabs-active"><a href="/login">Pieslēgties</a></li>
        <li><a href="/register">Reģistrēties</a></li>
    </ul>
    <div id="login">
        <form method="POST" action="{{ route('login') }}" novalidate>
            @csrf
            <input type="text" name="email" value="{{ old('email') }}" placeholder="E-pasts" required>
            @if ($errors->has('email'))
                <strong class="form-error">{{ $errors->first('email') }}</strong>
            @endif
            <input type="password" name="password" value="{{ old('password') }}" placeholder="Parole" required>
            @if ($errors->has('password'))
                <strong class="form-error">{{ $errors->first('password') }}</strong>
            @endif
            <button type="submit">Pieslēgties</button>
            <input id="remember" name="remember" type="checkbox" {{ old( 'remember') ? 'checked' : '' }} />
            <label for="remember">Atcerēties mani</label>
            <a href="/password/reset" class="forgot">Aizmirsi paroli?</a>
            {{-- <div class="reg-facebook">Reģistrēties ar <a href="https://www.facebook.com/" target="_blank">Facebook</a></div> --}}
        </form>
    </div>
</div>
<!-- enter -->

<div class="forgot-content popup" id="forgot">
    <div>
        <h2>Atjaunot paroli</h2>
        Ievadiet savu e-pasta adresi, un mēs Jums nosūtīsim paroles atjaunošanas saiti.
        <form action="" method="post" validate="true">
            <input type="text" placeholder="E-pasts" required="required">
            <button type="submit">Nosūtīt</button>
        </form>
    </div>
</div>
<!-- forgot-content -->
@endsection
