@extends('layouts.main')
@section('title', $user->firstname . ' ' . $user->lastname)
@section('content')
<div class="profile">
    <div class="profile-img">
        <div class="profile-big-icon" @if($user->picture) style="background-image:url({{ asset('storage/avatars/'.$user->picture) }})" @endif></div>
    </div>
    <div class="profile-descr">

        <div class="profile-info">
            <div class="profile-box">
                <span><strong>{{ $user->firstname }}</strong></span>
                <span><strong>{{ $user->lastname }}</strong></span>
            </div>
        </div>
        <!-- profile-info -->

        {{-- <div class="my-facebook"><a href="https://www.facebook.com/" target="_blank"></a>Mans<br />Facebook</div> --}}

        <div class="profile-info">
            <span>{{ $user->city }}</span>
            <span>{{ $user->interests }}</span>
            <span>{{ $user->motivation }}</span>
        </div>
        <!-- profile-info -->

        <div class="profile-info">
            <div class="profile-title">Vairāk par mani</div>
            <div class="profile-about">
                {{ $user->about }}
            </div>
        </div>
        <!-- profile-info -->

    </div>
    <!-- profile-descr -->
</div>
<!-- profile -->


<div class="comments">
    <h3>Komentāri</h3>

    <div class="add-comment">
        <form>
            <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
            <div class="emoji">
                <div class="add-emoji"></div>
                <div class="emojis">
                    @include('partials.emo')
                </div>
            </div>
            <div class="add-image"><input type="file" name="image"></div>
            <button data-commentable-type="user" data-commentable-id="{{ $user->id }}" class="add" type="button" disabled="disabled"></button>
        </form>
    </div>

    <div class="comments-list">
        @include('partials.comments', ['commentableId' => $user->id, 'commentableType' => 'user', 'comments' => App\Comment::getCommentsWithReplies($user)])
    </div>
    <!-- comments-list -->
</div>
<!-- comments -->
@endsection
