<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, user-scalable=yes">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet">
        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <script src="/js/script.js?529161421"></script>
        <script src="/js/jQueryRotate.js"></script>
        <script src="/js/autosize.min.js"></script>
        <script src="/js/jquery-simple-validator.min.js"></script>
        <script src="/fancybox/jquery.fancybox.min.js"></script>
        <link href="/css/vendor/froala_styles.min.css" rel="stylesheet" type="text/css" />
        <link href="/style.css?161436132" rel="stylesheet" type="text/css" />
        <link href="/fancybox/jquery.fancybox.min.css" rel="stylesheet" type="text/css" />
        <link href="/media-queries.css?161436132" rel="stylesheet" type="text/css" />
    </head>
    <body>
        @include('partials.header')
        <div class="page-content @if(!in_array(Route::currentRouteName(), ['home', 'user', 'feed', 'help-together', 'task'])) page-content-wide @endif">
            @include('partials.nav')
            <div class="container">
                <div class="content">
                    @yield('content')
                </div>
                <!-- content -->
            </div>
            <!-- container -->
        </div>
        <!-- page-content -->
        <div class="top-arrow"></div>
        <img src="/img/answers-ok.jpeg" width="600" class="answers-popup" alt="ok" style="display:none">
        <div class="c-delete popup" id="c-delete">
            <div>
                <h2>Vai tiešām dzēst?</h2>
                <form>
                    <button data-fancybox-close>Atcelt</button>
                    <button type="submit">Dzēst</button>
                </form>
            </div>
        </div>
    </body>
</html>
