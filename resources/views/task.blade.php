@extends('layouts.main')
@section('title', 'Spēles uzdevums')
@section('content')
    <div class="task-top">
        @if($taskStatus === 'bg-red')
            <div class="status status-r">Uzdevums nav izpildīts</div>
        @elseif($taskStatus === 'bg-orange')
            <div class="status status-o">Uzdevums tiek izskatīts</div>
        @elseif($taskStatus === 'bg-green')
            <div class="status status-g">Uzdevums pieņemts</div>
        @endif
        <a href="{{ route('tasks') }}" class="list">Uzdevumu saraksts<span></span></a>
        <span>{{ $task->game->name }}<span>{{ $task->day }}. diena</span></span>
    </div>

    <h2>{{ $task->name }}</h2>
    <span class="task-info">{{ $task->human_type }} uzdevums</span>

    <div class="task-descr fr-view">
        {!! $task->body !!}
    </div>
    <!-- task-descr -->

    @if($task->method)
        <a href="{{ route('method', $task->method) }}" class="task-method">Uzdevuma metode<span></span></a>
    @endif

    <div class="explanation fr-view">
        <h3>Skaidrojums ko rakstīt uzdevumā</h3>
        {!! $task->description !!}
    </div>
    <!-- explanation -->

    @if($task->questions)
        <div class="answers">
            <h3>Tava atbilde</h3>
            <ul>
                @foreach($task->questions as $k => $q)
                    <?php
                        $uAnswerPrivate = auth()->user()->getAnswer($task->id, $q['id'], true);
                        $uAnswerPublic= auth()->user()->getAnswer($task->id, $q['id']);
                    ?>
                    <li>
                        <h3>{{ $k+1 }}. {{ $q['question'] }}</h3>
                        <div>
                            <textarea data-private="true" data-file="fpriv{{ $q['id'] }}" data-id="{{ $q['id'] }}" placeholder="Atbilde nav publiski redzama">{{ optional($uAnswerPrivate)->body }}</textarea>
                            <div class="attachment" @if(optional($uAnswerPrivate)->file) style="opacity:1;background-image:url(/img/checked.png)" @endif><input class="fpriv{{ $q['id'] }}" type="file" accept="image/*"></div>
                        </div>
                        <div class="not-public">
                            <textarea data-id="{{ $q['id'] }}" data-file="fpub{{ $q['id'] }}" placeholder="Atbilde ir publiski redzama">{{ optional($uAnswerPublic)->body }}</textarea>
                            <div class="attachment" @if(optional($uAnswerPublic)->file) style="opacity:1;background-image:url(/img/checked.png)" @endif><input class="fpub{{ $q['id'] }}" type="file" accept="image/*"></div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @if(auth()->user()->getAnswers($task->id)->count())
                <div class="button sended">Atbilde nosūtīta</div>
            @else
                <button data-task-id="{{ $task->id }}" class="send-form" type="button" disabled="disabled">Nosūtīt atbildi</button>
                <div class="button sended" style="display: none">Atbilde nosūtīta</div>
            @endif
        </div>
        <!-- answers -->
    @endif

    <div class="answers-tabs">
        <ul>
            <li><a href="#other_answers">Citu atbildes<br>šim uzdevumam</a></li>
            <li><a href="#comments">Pajautā trenerim <span>Anonīmam jautājumam, raksti<br>spele@cilvekam.lv</span></a></li>
        </ul>
        <div id="other_answers" class="comments-list">
            <ul>
                @foreach($otherAnswers as $answers)
                    <li>
                        <div class="c-content">
                            <div class="c-img-big" id="c-fimg-big-{{ $answers[0]->id }}">
                                <div>
                                    @if($answers[0]->user->picture)
                                        <img src="{{ asset('storage/avatars/'.$answers[0]->user->picture) }}">
                                    @else
                                        <img src="/img/user.png">
                                    @endif
                                    <a href="{{ route('user', $answers[0]->user->id) }}">
                                        Uzraksti komentāru
                                    </a>
                                </div>
                            </div>
                            <a data-fancybox data-src="#c-fimg-big-{{ $answers[0]->id }}" href="javascript:;" class="c-img" @if($answers[0]->user->picture) style="background-image:url({{ asset('storage/avatars/'.$answers[0]->user->picture) }})" @endif></a>
                            <div class="c-top">
                                <a href="{{ route('user', $answers[0]->user->id) }}">
                                    {{ $answers[0]->user->firstname }} {{ $answers[0]->user->lastname }}
                                </a>
                            </div>
                            <div class="c-text">
                                @foreach($answers as $k => $answer)
                                    <span><p>{{ $k+1 }}. {!! nl2br(e($answer->body)) !!}</p></span>
                                    @if($answer->file)
                                        <div class="c-text-img answer">
                                            <a data-fancybox href="{{ asset('storage/answers/'.$answer->file) }}" data-width="800">
                                                <img src="{{ asset('storage/answers/'.$answer->file) }}">
                                            </a>
                                        </div>
                                    @endif
                                @endforeach
                                {{-- @if(auth()->user()->id === $answers[0]->user->id)
                                    @include('partials.edit_content', ['id' => $answers[0]->id, 'answers' => $answers])
                                @endif --}}
                                <a href="javascript:;" class="morelink">Vairāk</a>
                            </div>
                            {{-- <div class="c-reply">Atbildēt</div> --}}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div id="comments">
            <div class="add-comment">
                <form>
                    <textarea name="comment" placeholder="Ieraksti savu komentāru..."></textarea>
                    <div class="emoji">
                        <div class="add-emoji"></div>
                        <div class="emojis">
                            @include('partials.emo')
                        </div>
                    </div>
                    <div class="add-image"><input type="file" name="image"></div>
                    <button data-commentable-type="task" data-commentable-id="{{ $task->id }}" class="add" type="button" disabled="disabled"></button>
                </form>
            </div>
            <div class="comments-list">
                @include('partials.comments', ['commentableId' => $task->id, 'commentableType' => 'task', 'comments' => App\Comment::getCommentsWithReplies($task)])
            </div>
            <!-- comments-list -->
        </div>
    </div>
    <!-- answers-tabs -->
@endsection
