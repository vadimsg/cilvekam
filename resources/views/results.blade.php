@extends('layouts.main')
@section('title', 'Rezultātu tabula')
@section('content')
<h1>Rezultātu tabula</h1>

<div class="wide-bg grey-bg">

    <div class="results">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <th width="45%">
                        @if(Request::get('sort') === 'name:asc')
                            <a href="{{ route('results', ['sort' => 'name:desc']) }}">Dalībnieka vārds, uzvārds<div class="r-arrows r-arrows-up"></div></a>
                        @else
                            <a href="{{ route('results', ['sort' => 'name:asc']) }}">Dalībnieka vārds, uzvārds<div class="r-arrows r-arrows-down"></div></a>
                        @endif
                    </th>
                    <th>
                        @if(Request::get('sort') === 'points:asc')
                            <a href="{{ route('results', ['sort' => 'points:desc']) }}">Pats<div class="r-arrows r-arrows-up"></div></a>
                        @else
                            <a href="{{ route('results', ['sort' => 'points:asc']) }}">Pats<div class="r-arrows r-arrows-down"></div></a>
                        @endif
                    </th>
                    <th width="100%">
                        @if(Request::get('sort') === 'team_points:asc')
                            <a href="{{ route('results', ['sort' => 'team_points:desc']) }}">Komanda<div class="r-arrows r-arrows-up"></div></a>
                        @else
                            <a href="{{ route('results', ['sort' => 'team_points:asc']) }}">Komanda<div class="r-arrows r-arrows-down"></div></a>
                        @endif
                    </th>
                    <th width="29%">
                        Līmenis
                    </th>
                </tr>
                @foreach($users as $user)
                <tr>
                    <td>
                        <div class="r-img" @if($user->picture) style="background-image:url({{ asset('storage/avatars/'.$user->picture) }})" @endif></div>
                        <a href="{{ route('user', $user->id) }}">{{ $user->firstname }} {{ $user->lastname }}</a></td>
                    <td>{{ $user->points }}</td>
                    <td>{{ $user->team_points }}</td>
                    <td>
                        <div class="character-icon ch-{{ $user->rank }}-h"></div>{{ $user->rank_name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- results -->

</div>
<!-- wide-bg -->
@endsection
