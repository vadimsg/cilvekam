<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h3>Uzaicinājums pievienoties {{ $data['organization'] }}</h3>
            <p>Reģistrējaties ar šo <a href="{{ $data['link'] }}">saiti</a>.</p>
        </div>
    </body>
</html>
