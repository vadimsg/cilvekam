@extends('layouts.main')
@section('title', 'FAQ')
@section('content')
<h1>FAQ</h1>
<div class="wide-bg grey-bg">
    <div class="faq">
        @foreach($faqs as $k => $faq)
            <h3><a href="{{ route('faq', $k) }}">{{ $faq->title }}</a></h3>
            <div>
                {!! $faq->body !!}
            </div>
        @endforeach
    </div>
    <!-- faq -->
</div>
<!-- wide-bg -->
@endsection
