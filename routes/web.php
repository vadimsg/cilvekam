<?php

Auth::routes();


Route::get('test', function () {
    // $games = \App\Game::with('tasks')->get();

    // foreach ($games as $game) {
    //     if ($game->starts_at->toDateString() <= today()->toDateString()) {
    //         $lastGamesDay = \App\Task::whereGameId($game->id)->max('day');

    //         if ($game->active_day < $lastGamesDay) {
    //             $game->active_day += 1;
    //             $game->save();

    //             foreach ($game->tasks as $task) {
    //                 if ($game->active_day === $task->day) {
    //                     $task->hidden = false;
    //                     $task->save();
    //                 }
    //             }
    //         }
    //     }
    // }

    dd(true);
});

Route::middleware(['auth'])->group(function () {
    // Logout web user
    Route::get('logout', function () {
        auth('web')->logout();
        return redirect()->route('login');
    });

    Route::post('comment-delete', function () {
        $commentId = request()->input('id');
        App\Comment::destroy($commentId);

        return response()->json(['status' => 'ok']);
    });

    Route::post('comment-update', function () {
        $file = request()->file('image');
        $commentId = request()->input('commentId');
        $commentableId = request()->input('commentableId');
        $commentableType = request()->input('commentableType');
        $message = request()->input('message');

        $comment = App\Comment::findOrFail($commentId);
        $comment->body = $message;
        $comment->save();

        $model = App::make('App\\' . ucfirst($commentableType));
        $commentable = $model::findOrFail($commentableId);
        $commentable->comments()->save($comment);
        $commentable->load('comments.user');

        $view = view('partials.comments', [
            'commentableId' => $commentableId,
            'commentableType' => $commentableType,
            'comments' => App\Comment::getCommentsWithReplies($commentable)
        ])->render();

        return response()->json(['comments' => $view]);
    });

    // Save user comment
    Route::post('comment', function () {
        $file = request()->file('image');
        $parentId = request()->input('parentId');
        $commentableId = request()->input('commentableId');
        $commentableType = request()->input('commentableType');
        $message = request()->input('message');

        $fileName = null;

        if ($file) {
            $image = \Image::make($file);
            $image->orientate();
            $image->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $extension = $file->getClientOriginalExtension();
            $fileName = md5(microtime()) . '.' . $extension;
            $image->save(storage_path('app/public/comments/'. $fileName));
        }

        if ($parentId === 'undefined') {
            $parentId = null;
        }

        $comment = new App\Comment([
            'parent_id' => $parentId,
            'user_id' => auth()->user()->id,
            'organization_id' => auth()->user()->organization_id,
            'body' => $message,
            'file' => $fileName
        ]);

        $model = App::make('App\\' . ucfirst($commentableType));
        $commentable = $model::findOrFail($commentableId);
        $commentable->comments()->save($comment);
        $commentable->load('comments.user');

        $view = view('partials.comments', [
            'commentableId' => $commentableId,
            'commentableType' => $commentableType,
            'comments' => App\Comment::getCommentsWithReplies($commentable)
        ])->render();

        return response()->json(['comments' => $view]);
    });

    // Save user answers
    Route::post('answers', function () {
        $answers = request()->input('answers');

        if ($answers) {
            $answers = json_decode($answers, true);
        }

        $task = App\Task::findOrFail($answers[0]['taskId']);

        $points = $task->type === 'REQUIRED' ? 20 : 10;
        $bonusPoints = 0;

        foreach ($answers as $answer) {
            if ($answer['private'] === true) {
                $file = request()->file($answer['id'] . '_private');
            } else {
                $file = request()->file($answer['id'] . '_public');
            }

            $fileName = null;

            if ($file) {
                $image = \Image::make($file);
                $image->orientate();
                $image->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $extension = $file->getClientOriginalExtension();
                $fileName = md5(microtime()) . '.' . $extension;
                $image->save(storage_path('app/public/answers/'. $fileName));
            }

            App\Answer::create([
                'organization_id' => auth()->user()->organization_id,
                'user_id' => auth()->user()->id,
                'task_id' => $answer['taskId'],
                'question_id' => $answer['id'],
                'body' => $answer['body'],
                'private' => $answer['private'] === true,
                'file' => $fileName
            ]);

            if ($answer['private'] !== true) {
                $bonusPoints = 10;
            }
        }

        if ($task->day === 0) {
            $points = 0;
            $bonusPoints = 0;
        }

        $points = $points + $bonusPoints;

        auth()->user()->points += $points;
        auth()->user()->save();

        return response()->json(['status' => 'ok']);
    });

    // User game tasks
    Route::get('tasks', function () {
        $user = auth()->user();

        if ($gameId = request()->get('game_id')) {
            $game = App\Game::findOrFail($gameId);

            if ($game->organization_id === $user->organization_id) {
                $user->game_id = $gameId;
                $user->save();

                return redirect()->to('tasks');
            }
        }

        $user->load('game.tasks');

        $game = $user->game;
        $tasks = $user->game ? $user->game->tasks->filter(function ($value, $key) {
            return !$value->hidden;
        }) : [];

        return view('tasks', compact('game', 'tasks'));
    })->name('tasks');

    // Game task
    Route::get('task/{id}', function ($id) {
        $task = App\Task::with([
            'game',
            'userAnswers.user',
            'userAnswers.comments.user',
            'comments.user'
        ])->findOrFail($id);

        if ($task->hidden || $task->organization_id !== auth()->user()->organization_id) {
            abort(404);
        }

        $otherAnswers = $task->userAnswers
            ->filter(function ($answer, $key) {
                return !$answer->private;
            })
            ->groupBy('user_id');

        $taskStatus = auth()->user()->getTaskStatus($task);

        return view('task', compact('task', 'otherAnswers', 'taskStatus'));
    })->name('task');

    Route::get('results', function (Illuminate\Http\Request $request) {
        $users = [];

        if (auth()->user()->game) {
            $query = App\User::where('organization_id', auth()->user()->game->organization_id)
                ->where('game_id', auth()->user()->game->id);

            if ($sorting = $request->get('sort')) {
                $attribute = explode(':', $sorting)[0];
                $sort = explode(':', $sorting)[1];

                if ($attribute === 'name' && in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy('firstname', $sort);
                }

                if ($attribute === 'points' && in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy('points', $sort);
                }
            } else {
                $query->orderBy('points', 'desc');
            }

            $users = $query->get();

            if ($sorting) {
                if ($attribute === 'team_points' && in_array($sort, ['asc', 'desc'])) {
                    if ($sort == 'asc') {
                        $users = $users->sortBy('team_points');
                    } else {
                        $users = $users->sortByDesc('team_points');
                    }
                }
            }
        }

        return view('results', compact('users'));
    })->name('results');

    Route::get('feed', function () {
        $feeds = App\Feed::with(['user', 'organization', 'comments'])
            ->where('organization_id', auth()->user()->organization_id)
            ->whereNull('type')
            ->orderByDesc('id')
            ->get();

        return view('feed', compact('feeds'));
    })->name('feed');

    Route::get('methods', function () {
        $user = auth()->user();
        $user->load('game.tasks');

        $tasks = $user->game->tasks->filter(function ($value, $key) {
            return $value->method && !$value->hidden;
        });

        return view('methods', compact('tasks'));
    })->name('methods');

    Route::get('method/{method}', function ($method) {
        if (Storage::disk('public')->exists($method)) {
            return Storage::download($method);
        } else {
            abort(404);
        }
    })->name('method');

    Route::get('faq/{faqId?}', function ($faqId = null) {
        $faqs = App\Faq::orderByDesc('id')->get();
        return view('faq', compact('faqs'));
    })->name('faq');

    Route::get('rules', function () {
        $page = App\Page::find(1);
        return view('rules', compact('page'));
    })->name('rules');

    Route::get('help-together', function () {
        $donations = App\Feed::with(['user', 'organization', 'comments'])
            ->where('organization_id', auth()->user()->organization_id)
            ->whereType('DONATIONS')
            ->orderByDesc('id')
            ->get();

        return view('help_together', compact('donations'));
    })->name('help-together');

    Route::get('user/{id}', function ($id) {
        $user = App\User::with(['comments.user'])->findOrFail($id);

        return view('user', compact('user'));
    })->name('user');

    // Update   user profile
    Route::post('update-profile', function (Illuminate\Http\Request $request) {
        $validation = [
            'firstname' => 'required',
            'lastname' => 'required',
            'city' => 'required',
            'interests' => 'required',
            'motivation' => 'required',
        ];

        $request->validate($validation);

        $inputs = $request->except(['_token', 'photo', 'password', 'password_confirmation']);

        auth()->user()
            ->fill($inputs)
            ->save();

        if ($picture = $request->file('picture')) {
            $extension = $picture->getClientOriginalExtension();
            $fileName = md5(time()) . '.' . $extension;
            $resizedImage = Image::make($picture->getRealPath())->fit(150)->encode('jpg');
            Storage::disk('public')->put('avatars/'.$fileName, $resizedImage);
            auth()->user()->picture = $fileName;
            auth()->user()->save();
        }

        return back();
    })->name('update-profile');

    // User profile
    Route::get('{name?}', function () {
        $user = auth()->user();
        $user->load(['comments.user']);
        $games = $user->games;

        return view('profile', compact('games', 'user'));
    })->where('name', '|home')->name('home');
});
