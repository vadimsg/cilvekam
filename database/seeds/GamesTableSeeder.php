<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $orgIds = \App\Organization::pluck('id');

        foreach ($orgIds as $id) {
            App\Game::create([
                'name' => $faker->sentence(3),
                'description' => $faker->sentence(),
                'organization_id' => $id,
                'language' => 'lv',
            ]);
        }
    }
}
