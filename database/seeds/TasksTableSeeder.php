<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $games = \App\Game::all();

        for ($g = 0; $g < $games->count(); $g++) {
            for ($i = 0; $i < 10; $i++) {
                \App\Task::create([
                    'name' => $faker->sentence(),
                    'organization_id' => $games[$g]->organization_id,
                    'game_id' => $games[$g]->id,
                    'type' => 'REQUIRED',
                    'day' => $i+1,
                    'body' => $faker->text(800),
                    'description' => $faker->text(1400),
                ]);
            }
        }
    }
}
