<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'email' => "root@root",
            'password' => bcrypt('root'),
            'is_root' => true,
            'created_at' => Carbon\Carbon::now(),
        ]);

        DB::table('admins')->insert([
            'email' => "bite@bite",
            'password' => bcrypt('bite'),
            'is_root' => false,
            'organization_id' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);

        DB::table('admins')->insert([
            'email' => "taxify@taxify",
            'password' => bcrypt('taxify'),
            'is_root' => false,
            'organization_id' => 2,
            'created_at' => Carbon\Carbon::now(),
        ]);
    }
}
