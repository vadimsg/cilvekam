<?php

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organizations = ['Bite', 'Taxify'];

        foreach ($organizations as $organization) {
            App\Organization::create([
                'name' => $organization
            ]);
        }
    }
}
