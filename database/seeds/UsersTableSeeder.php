<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            DB::table('users')->insert([
                'firstname' => "User1",
                'lastname' => "User1",
                'email' => 'user1@user1',
                'password' => bcrypt('user1'),
                'organization_id' => 1,
                'created_at' => Carbon\Carbon::now(),
            ]);

            DB::table('users')->insert([
                'firstname' => "User2",
                'lastname' => "User2",
                'email' => 'user2@user2',
                'password' => bcrypt('user2'),
                'organization_id' => 2,
                'created_at' => Carbon\Carbon::now(),
            ]);
        }
    }
}
