<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const TYPES = [
        'REQUIRED' => 'Pamata',
        'ADDITIONAL' => 'Bonus',
        'NOT_REQUIRED' => 'Nav obligāts',
    ];

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'questions' => 'array',
        'hidden' => 'boolean'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    /**
     * Get the task's type.
     *
     * @param  string  $value
     * @return string
     */
    public function getHumanTypeAttribute()
    {
        return self::TYPES[$this->type];
    }

    /**
     * Get the organization that owns the task.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    /**
     * Get the game that owns the task.
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    /**
     * Get task answers
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get all of the task's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get the user's answers.
     */
    public function userAnswers()
    {
        return $this->hasMany('App\Answer')
            ->where('organization_id', auth()->user()->organization_id);
    }
}
