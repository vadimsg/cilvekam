<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    const ANSWERED_PRIV = 20;
    const ANSWERED_PUB = 20;
    const POSTED_IMAGE = 10;
    const POSTED_STORY = 10;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'private' => 'boolean',
    ];

    /**
     * Get the user that owns the answer.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the task that owns the answer.
     */
    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    /**
     * Get the organization that owns the answer.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    /**
     * Get all of the answer's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
