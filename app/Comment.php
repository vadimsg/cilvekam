<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get comment with emojies
     */
    public function getBodyAttribute($body)
    {
        $emojies = [
            ':)' => '<img src="/img/emoji/happy.svg" class="emo" alt="happy">',
            ':D' => '<img src="/img/emoji/happy2.svg" class="emo" alt="happy2">',
            ':|' => '<img src="/img/emoji/confused.svg" class="emo" alt="confused">',
            ';/' => '<img src="/img/emoji/thinking.svg" class="emo" alt="thinking">',
            '8)' => '<img src="/img/emoji/favorite.svg" class="emo" alt="favorite">',
            ':(' => '<img src="/img/emoji/sad.svg" class="emo" alt="sad">'
        ];

        return $body;
        return str_replace(array_keys($emojies), $emojies, htmlspecialchars($body, ENT_QUOTES));
    }

    /**
     * Create comment tree
     */
    public static function getCommentsWithReplies($commentable)
    {
        $comments = $commentable->comments;

        $parents = $comments->filter(function ($value, $key) {
            return $value->parent_id === null;
        });

        $children = $comments->filter(function ($value, $key) {
            return $value->parent_id !== null;
        });

        foreach ($parents as $parent) {
            $parent->attributes['children'] = $children->where(
                'parent_id',
                $parent->id
            );
        }

        return $parents;
    }

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get the user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the organization that owns the comment.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }
}
