<?php

namespace App;

use App\Mail\UserInvited;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (!app()->runningInConsole()) {
            self::creating(function ($model) {
                $organization = \App\Organization::findOrFail($model->attributes['organization_id']);
                $model->attributes['token'] = md5(microtime(true) * 1000);

                $data = [
                    'organization' => $organization->name,
                    'link' => secure_url('/register?token='.$model->attributes['token'])
                ];

                Mail::to($model->attributes['email'])->queue(new UserInvited($data));
            });
        }
    }

    /**
     * Get the organization that owns the game.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }
}
