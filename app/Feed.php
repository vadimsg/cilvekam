<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get all of the feed's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get the user that owns the feed.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the organization that owns the feed.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }
}
