<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Froala\NovaFroalaField\Jobs\PruneStaleAttachments;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('queue:work --tries=3')
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->call(function () {
            (new PruneStaleAttachments)();
        })->daily();

        $schedule->call(function () {
            $games = \App\Game::with('tasks')->get();

            foreach ($games as $game) {
                if ($game->starts_at->toDateString() <= today()->toDateString()) {
                    $lastGamesDay = \App\Task::whereGameId($game->id)->max('day');

                    if ($game->active_day < $lastGamesDay) {
                        $game->active_day += 1;
                        $game->save();

                        foreach ($game->tasks as $task) {
                            if ($game->active_day === $task->day) {
                                $task->hidden = false;
                                $task->save();
                            }
                        }
                    }
                }
            }
        })->dailyAt('00:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
