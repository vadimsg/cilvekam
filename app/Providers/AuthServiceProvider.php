<?php

namespace App\Providers;

use App\Faq;
use App\User;
use App\Game;
use App\Task;
use App\Page;
use App\Admin;
use App\Invite;
use App\Answer;
use App\Comment;
use App\Organization;
use App\Policies\FaqPolicy;
use App\Policies\PagePolicy;
use App\Policies\GamePolicy;
use App\Policies\UserPolicy;
use App\Policies\TaskPolicy;
use App\Policies\AdminPolicy;
use App\Policies\InvitePolicy;
use App\Policies\AnswerPolicy;
use App\Policies\CommentPolicy;
use App\Policies\OrganizationPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Admin::class => AdminPolicy::class,
        User::class => UserPolicy::class,
        Game::class => GamePolicy::class,
        Task::class => TaskPolicy::class,
        Invite::class => InvitePolicy::class,
        Comment::class => CommentPolicy::class,
        Organization::class => OrganizationPolicy::class,
        Faq::class => FaqPolicy::class,
        Answer::class => AnswerPolicy::class,
        Page::class => PagePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
