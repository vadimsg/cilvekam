<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/tasks';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users'
            ],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'terms' => ['accepted']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // $invite = \App\Invite::where('token', $data['invite_token'])
        //     ->where('used', false)
        //     ->first();

        // if (!$invite) {
        //     abort(400, 'Invalid token.');
        // }

        $user = User::create([
            // 'organization_id' => $invite->organization_id,
            'organization_id' => 10,
            // 'game_id' => $invite->game_id,
            'game_id' => 10,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => Hash::make($data['password'])
        ]);

        // $invite->used = true;
        // $invite->save();

        return $user;
    }

    public function showRegistrationForm()
    {
        // $game = \App\Game::find(5);
        $rules = \App\Page::find(1);
        $data = ['email' => null, 'error' => false, 'rules' => $rules, 'game' => false];
        return view('auth.register', $data);

        $token = request()->get('token');
        $invite = \App\Invite::where('token', $token)
            ->where('used', false)
            ->first();

        if ($token && $invite) {
            $data = ['email' => $invite->email, 'error' => false];
        } elseif ($token && !$invite) {
            $data = ['email' => null, 'error' => false];
        } else {
            $data = ['email' => null, 'error' => true];
        }

        return view('auth.register', $data);
    }
}
