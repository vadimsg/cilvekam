<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'team_points_is_hidden' => 'boolean',
        'donation_is_hidden' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'starts_at',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    /**
     * Scope a query to only include users organization games.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMy($query)
    {
        if (auth()->guard('admin')->user()->is_root) {
            return $query;
        }

        return $query->where('organization_id', auth()->guard('admin')->user()->organization_id);
    }

    /**
     * Get available points
     */
    public function getPointsAvailableAttribute()
    {
        $tasks = $this->tasks()->where('day', '!=', 0)->get();
        $pointsTotal = 0;

        foreach ($tasks as $task) {
            if ($task->type === 'REQUIRED') {
                $pointsTotal += 20;
            } else {
                $pointsTotal += 10;
            }
        }

        return $pointsTotal;
    }

    /**
     * Get available points
     */
    public function getUserTotalPoints()
    {
        // TODO: check if relation not loaded
        return $this->users()->sum('points');
    }

    /**
     * Get available points
     */
    public function getDonationProgress()
    {
        $points = $this->getUserTotalPoints();

        if ($points < 1) {
            return 0;
        }

        $progress = intval($points/5000*100);

        return $progress > 100 ? 100 : $progress;
    }

    /**
     * Get the users that play this game
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Get the organization that owns the game.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    /**
     * Get the tasks that owns the game.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task')->orderByDesc('day');
    }
}
