<?php

namespace App\Policies;

use App\User;
use App\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Admin  $user
     * @param  \App\Admin  $model
     * @return mixed
     */
    public function view(Admin $user, User $model)
    {
        return $user->organization_id === $model->organization_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Admin  $user
     * @param  \App\Admin  $model
     * @return mixed
     */
    public function update(Admin $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Admin  $user
     * @param  \App\Admin  $model
     * @return mixed
     */
    public function delete(Admin $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Admin  $user
     * @param  \App\Admin  $model
     * @return mixed
     */
    public function restore(Admin $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Admin  $user
     * @param  \App\Admin  $model
     * @return mixed
     */
    public function forceDelete(Admin $user, User $model)
    {
        //
    }
}
