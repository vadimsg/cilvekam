<?php

namespace App\Policies;

use App\Admin;
use App\Answer;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnswerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the answer.
     *
     * @param  \App\Admin  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function view(Admin $user, Answer $answer)
    {
        if ($user->is_root) {
            return true;
        }

        return $user->organization_id === $answer->organization_id;
    }

    /**
     * Determine whether the user can create answers.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the answer.
     *
     * @param  \App\Admin  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function update(Admin $user, Answer $answer)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the answer.
     *
     * @param  \App\Admin  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function delete(Admin $user, Answer $answer)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the answer.
     *
     * @param  \App\Admin  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function restore(Admin $user, Answer $answer)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the answer.
     *
     * @param  \App\Admin  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function forceDelete(Admin $user, Answer $answer)
    {
        return false;
    }
}
