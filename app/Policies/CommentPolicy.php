<?php

namespace App\Policies;

use App\Admin;
use App\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the comment.
     *
     * @param  \App\Admin  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function view(Admin $user, Comment $comment)
    {
        return $user->organization_id === $comment->organization_id;
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\Admin  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function update(Admin $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\Admin  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function delete(Admin $user, Comment $comment)
    {
        return $user->organization_id === $comment->organization_id;
    }

    /**
     * Determine whether the user can restore the comment.
     *
     * @param  \App\Admin  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function restore(Admin $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the comment.
     *
     * @param  \App\Admin  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function forceDelete(Admin $user, Comment $comment)
    {
        //
    }
}
