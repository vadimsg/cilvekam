<?php

namespace App\Policies;

use App\Admin;
use App\Invite;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvitePolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the invite.
     *
     * @param  \App\Admin  $user
     * @param  \App\Invite  $invite
     * @return mixed
     */
    public function view(Admin $user, Invite $invite)
    {
        return $user->organization_id === $invite->organization_id;
    }

    /**
     * Determine whether the user can create invites.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the invite.
     *
     * @param  \App\Admin  $user
     * @param  \App\Invite  $invite
     * @return mixed
     */
    public function update(Admin $user, Invite $invite)
    {
        return $user->organization_id === $invite->organization_id;
    }

    /**
     * Determine whether the user can delete the invite.
     *
     * @param  \App\Admin  $user
     * @param  \App\Invite  $invite
     * @return mixed
     */
    public function delete(Admin $user, Invite $invite)
    {
        return $user->organization_id === $invite->organization_id;
    }

    /**
     * Determine whether the user can restore the invite.
     *
     * @param  \App\Admin  $user
     * @param  \App\Invite  $invite
     * @return mixed
     */
    public function restore(Admin $user, Invite $invite)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the invite.
     *
     * @param  \App\Admin  $user
     * @param  \App\Invite  $invite
     * @return mixed
     */
    public function forceDelete(Admin $user, Invite $invite)
    {
        //
    }
}
