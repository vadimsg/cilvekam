<?php

namespace App\Policies;

use App\Task;
use App\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\Admin  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(Admin $user, Task $task)
    {
        return $user->organization_id === $task->organization_id;
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\Admin  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(Admin $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\Admin  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(Admin $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can restore the task.
     *
     * @param  \App\Admin  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(Admin $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the task.
     *
     * @param  \App\Admin  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(Admin $user, Task $task)
    {
        //
    }
}
