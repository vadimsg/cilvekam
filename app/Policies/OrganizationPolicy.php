<?php

namespace App\Policies;

use App\Admin;
use App\Organization;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrganizationPolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the organization.
     *
     * @param  \App\Admin  $user
     * @param  \App\Organization  $organization
     * @return mixed
     */
    public function view(Admin $user, Organization $organization)
    {
        //
    }

    /**
     * Determine whether the user can create organizations.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the organization.
     *
     * @param  \App\Admin  $user
     * @param  \App\Organization  $organization
     * @return mixed
     */
    public function update(Admin $user, Organization $organization)
    {
        //
    }

    /**
     * Determine whether the user can delete the organization.
     *
     * @param  \App\Admin  $user
     * @param  \App\Organization  $organization
     * @return mixed
     */
    public function delete(Admin $user, Organization $organization)
    {
        //
    }

    /**
     * Determine whether the user can restore the organization.
     *
     * @param  \App\Admin  $user
     * @param  \App\Organization  $organization
     * @return mixed
     */
    public function restore(Admin $user, Organization $organization)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the organization.
     *
     * @param  \App\Admin  $user
     * @param  \App\Organization  $organization
     * @return mixed
     */
    public function forceDelete(Admin $user, Organization $organization)
    {
        //
    }

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        //
    }
}
