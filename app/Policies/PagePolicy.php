<?php

namespace App\Policies;

use App\Admin;
use App\Page;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the page.
     *
     * @param  \App\Admin  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function view(Admin $user, Page $page)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can create pages.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the page.
     *
     * @param  \App\Admin  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function update(Admin $user, Page $page)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the page.
     *
     * @param  \App\Admin  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function delete(Admin $user, Page $page)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the page.
     *
     * @param  \App\Admin  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function restore(Admin $user, Page $page)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the page.
     *
     * @param  \App\Admin  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function forceDelete(Admin $user, Page $page)
    {
        return false;
    }

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        if ($user->is_root) {
            return true;
        }
    }
}
