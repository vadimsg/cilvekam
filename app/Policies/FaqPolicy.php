<?php

namespace App\Policies;

use App\Faq;
use App\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class FaqPolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the faq.
     *
     * @param  \App\Admin  $user
     * @param  \App\Faq  $faq
     * @return mixed
     */
    public function view(Admin $user, Faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can create faqs.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the faq.
     *
     * @param  \App\Admin  $user
     * @param  \App\Faq  $faq
     * @return mixed
     */
    public function update(Admin $user, Faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can delete the faq.
     *
     * @param  \App\Admin  $user
     * @param  \App\Faq  $faq
     * @return mixed
     */
    public function delete(Admin $user, Faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can restore the faq.
     *
     * @param  \App\Admin  $user
     * @param  \App\Faq  $faq
     * @return mixed
     */
    public function restore(Admin $user, Faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the faq.
     *
     * @param  \App\Admin  $user
     * @param  \App\Faq  $faq
     * @return mixed
     */
    public function forceDelete(Admin $user, Faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        //
    }
}
