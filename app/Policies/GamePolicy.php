<?php

namespace App\Policies;

use App\Admin;
use App\Game;
use Illuminate\Auth\Access\HandlesAuthorization;

class GamePolicy
{
    use HandlesAuthorization;

    /**
     * Root admin
     */
    public function before(Admin $user, $ability)
    {
        if ($user->is_root) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the game.
     *
     * @param  \App\Admin  $user
     * @param  \App\Game  $game
     * @return mixed
     */
    public function view(Admin $user, Game $game)
    {
        return $user->organization_id === $game->organization_id;
    }

    /**
     * Determine whether the user can create games.
     *
     * @param  \App\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the game.
     *
     * @param  \App\Admin  $user
     * @param  \App\Game  $game
     * @return mixed
     */
    public function update(Admin $user, Game $game)
    {
        //
    }

    /**
     * Determine whether the user can delete the game.
     *
     * @param  \App\Admin  $user
     * @param  \App\Game  $game
     * @return mixed
     */
    public function delete(Admin $user, Game $game)
    {
        //
    }

    /**
     * Determine whether the user can restore the game.
     *
     * @param  \App\Admin  $user
     * @param  \App\Game  $game
     * @return mixed
     */
    public function restore(Admin $user, Game $game)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the game.
     *
     * @param  \App\Admin  $user
     * @param  \App\Game  $game
     * @return mixed
     */
    public function forceDelete(Admin $user, Game $game)
    {
        //
    }
}
