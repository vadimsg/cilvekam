<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Froala\NovaFroalaField\Froala;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Fourstacks\NovaRepeatableFields\Repeater;

class Task extends Resource
{
    // Navbar group
    public static $group = 'Game';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Task';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['name'];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'game' => ['name'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $organizations = \App\Organization::my()->pluck('name', 'id');
        $games = \App\Game::my()->pluck('name', 'id');

        return [
            Select::make('Organization', 'organization_id')
                ->options($organizations)
                ->rules('required')
                ->onlyOnForms(),

            Select::make('Game', 'game_id')
                ->options($games)
                ->rules('required')
                ->onlyOnForms(),

            BelongsTo::make('Game')
                ->displayUsing(function ($game) {
                    return strtoupper($game->name);
                })
                ->sortable()
                ->onlyOnIndex(),

            Text::make('Name')
                ->sortable()
                ->rules('required'),

            Select::make('Type')
                ->options(\App\Task::TYPES)
                ->displayUsingLabels()
                ->sortable()
                ->rules('required'),

            Text::make('Card label')
                ->hideFromIndex(),

            Number::make('Day')
                ->min(0)
                ->sortable()
                ->rules('required'),

            DateTime::make('Created At')
                ->sortable()
                ->format('DD MMM YYYY, H:m')
                ->onlyOnIndex(),

            Froala::make('Description', 'body')->withFiles('public'),
            Froala::make('What to do in task', 'description')->withFiles('public'),

            File::make('Method')->disk('public'),

            Repeater::make('Questions')
                ->help(
                    'Katram jautājumam ierakstīt unikālu ID identifikatoru, piem, "1", "2", "3" u.tml.<br>Pēc uzdevuma pievienošanas nedrīkst mainīt jautājuma identifikatoru.'
                )
                ->addField([
                    'label' => 'ID',
                    'name' => 'id',
                    'width' => 'w-1/6'
                ])
                ->addField([
                    'label' => 'Question',
                    'name' => 'question',
                    'width' => 'w-5/6'
                ])
                ->addButtonText('Add question')
                ->onlyOnForms(),

            Boolean::make('Task hidden', 'hidden')->sortable()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user('admin')->is_root) {
            return $query;
        }

        return $query->where('organization_id', $request->user('admin')->organization_id);
    }
}
