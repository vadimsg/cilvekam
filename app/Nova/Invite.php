<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;

class Invite extends Resource
{
    // Navbar group
    public static $group = 'Game';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Invite';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'email';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['email'];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $organizations = \App\Organization::my()->pluck('name', 'id');
        $games = \App\Game::my()->pluck('name', 'id');

        return [
            Select::make('Organization', 'organization_id')
                ->options($organizations)
                ->rules('required')
                ->onlyOnForms(),

            Select::make('Game', 'game_id')
                ->options($games)
                ->rules('required')
                ->onlyOnForms(),

            Text::make('Email')
                ->sortable()
                ->rules(
                    'required',
                    'email',
                    'unique:users,email',
                    'unique:invites,email',
                    'max:255'
                ),

            BelongsTo::make('Organization')
                ->displayUsing(function ($organization) {
                    return strtoupper($organization->name);
                })
                ->sortable()
                ->onlyOnIndex(),

            Boolean::make('Used')
                ->sortable()
                ->onlyOnIndex(),

            DateTime::make('Created At')
                ->sortable()
                ->format('DD MMM YYYY, H:m')
                ->onlyOnIndex(),

            Text::make('token')->onlyOnDetail()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user('admin')->is_root) {
            return $query;
        }

        return $query->where('organization_id', $request->user('admin')->organization_id);
    }
}
