<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Froala\NovaFroalaField\Froala;
use Laravel\Nova\Http\Requests\NovaRequest;

class Feed extends Resource
{
    // Navbar group
    public static $group = 'Content';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Feed';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['body'];

    // Custom label
    public static function label() {
        return 'Čalotava';
    }

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'organization' => ['name'],
        'user' => ['firstname', 'lastname', 'email']
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Organization')
                ->displayUsing(function ($organization) {
                    return $organization->name;
                })
                ->sortable(),

            Select::make('User', 'user_id')
                ->options(['2' => 'Zuarguss Zarmass'])
                ->displayUsingLabels()
                ->sortable()
                ->rules('required'),

            // BelongsTo::make('User')
            //     ->displayUsing(function ($user) {
            //         return $user->firstname;
            //     })
            //     ->sortable(),

            Froala::make('Body')->withFiles('public'),

            DateTime::make('Created At')
                ->firstDayOfWeek(1)
                ->sortable()
                ->format('DD MMM YYYY, H:m')
                ->onlyOnIndex()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user('admin')->is_root) {
            return $query->whereNull('type');
        }

        return $query->where('organization_id', $request->user('admin')->organization_id)
            ->whereNull('type');
    }
}
