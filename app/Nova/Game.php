<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;

class Game extends Resource
{
    // Navbar group
    public static $group = 'Game';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Game';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['name'];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $organizations = \App\Organization::my()->pluck('name', 'id');

        return [
            Select::make('Organization', 'organization_id')
                ->options($organizations)
                ->rules('required')
                ->onlyOnForms(),

            Select::make('Language')
                ->options(['lv' => 'Latviešu'])
                ->displayUsing(function ($organization) {
                    return strtoupper($organization);
                })
                ->sortable()
                ->rules('required'),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Description')->onlyOnForms(),

            BelongsTo::make('Organization')
                ->displayUsing(function ($organization) {
                    return strtoupper($organization->name);
                })
                ->sortable()
                ->onlyOnIndex(),

            Date::make('Starts At')
                ->firstDayOfWeek(1)
                ->sortable()
                ->rules('required')
                ->format('DD MMM YYYY'),

            Number::make('Max points', 'max_points')
                ->min(0)
                ->hideFromIndex(),

            Number::make('Max team points', 'max_points_team')
                ->min(0)
                ->hideFromIndex(),

            Boolean::make('Team points hidden', 'team_points_is_hidden')->sortable(),
            Boolean::make('Donations hidden', 'donation_is_hidden')->sortable(),


            DateTime::make('Created At')
                ->firstDayOfWeek(1)
                ->sortable()
                ->format('DD MMM YYYY, H:m')
                ->onlyOnIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user('admin')->is_root) {
            return $query;
        }

        return $query->where('organization_id', $request->user('admin')->organization_id);
    }
}
