<?php

namespace App\Observers;

use App\Task;

class TaskObserver
{
    /**
     * Handle the answer "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(Task $task)
    {
        // $task->load('game');

        // $activeDay = $task->game->active_day;

        // if ($task->game->starts_at->addDays($activeDay)->toDateString() <= today()->toDateString()) {
        //     $task->hidden = false;
        //     $task->save();
        // }
    }

    /**
     * Handle the answer "updated" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function updated(Task $task)
    {
        //
    }

    /**
     * Handle the answer "deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function deleted(Task $task)
    {
        //
    }

    /**
     * Handle the answer "restored" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function restored(Task $task)
    {
        //
    }

    /**
     * Handle the answer "force deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function forceDeleted(Task $task)
    {
        //
    }
}
