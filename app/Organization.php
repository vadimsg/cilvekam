<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Scope a query to only include users organization.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMy($query)
    {
        if (auth()->guard('admin')->user()->is_root) {
            return $query;
        }

        return $query->where('id', auth()->guard('admin')->user()->organization_id);
    }
}
