<?php

namespace App;

use Carbon\Carbon;
use Laravel\Nova\Nova;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Always eager load these relationships
     *
     * @var array
     */
    protected $with = ['game'];

    /**
     * Bootstrap any model services.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        // Nova::serving(function () {
            static::saved(function ($user) {
                $prevFriendid = $user->getOriginal('friend_id');

                if ($prevFriendid !== $user->friend_id) {
                    if ($user->friend_id) {
                        $friend = self::findOrFail($user->friend_id);
                        $friend->friend_id = $user->id;
                        $friend->unsetEventDispatcher();
                        $friend->save();
                    } else {
                        $friend = self::findOrFail($prevFriendid);
                        $friend->friend_id = null;
                        $friend->unsetEventDispatcher();
                        $friend->save();
                    }
                }
            });
        // });
    }

    /**
     * Get team rank.
     */
    public function getTeamPointsAttribute()
    {
        if ($this->friend_id) {
            $friend = self::findOrFail($this->friend_id);
            return $friend->points + $this->points;
        }

        return 0;
    }

    /**
     * Get users rank.
     */
    public function getRankAttribute()
    {
        $points = $this->attributes['points'];

        if ($points >= 260) {
            $rank = 7;
        } else if ($points >= 200) {
            $rank = 6;
        } else if ($points >= 150) {
            $rank = 5;
        } else if ($points >= 100) {
            $rank = 4;
        } else if ($points >= 60) {
            $rank = 3;
        } else if ($points >= 30) {
            $rank = 2;
        } else {
            $rank = 1;
        }

        return $rank;
    }

    /**
     * Get users rank name.
     */
    public function getRankNameAttribute()
    {
        $points = $this->attributes['points'];

        if ($points >= 250) {
            $rank = 'Homo Felicitius';
        } else if ($points >= 200) {
            $rank = 'Aizrautīgais Burvis';
        } else if ($points >= 150) {
            $rank = 'Burvis';
        } else if ($points >= 100) {
            $rank = 'Burvju māceklis';
        } else if ($points >= 60) {
            $rank = 'Viedais Rūķis';
        } else if ($points >= 30) {
            $rank = 'Rūķis';
        } else {
            $rank = 'Mazais Rūķis';
        }

        return $rank;
    }

    /**
     * Get the game that the user plays.
     */
    public function getPointsProgressAttribute()
    {
        if ($this->points < 1 || $this->game->max_points < 1) {
            return 0;
        }

        $progress = intval($this->points/$this->game->max_points*100);

        return $progress > 100 ? 100 : $progress;
    }

    /**
     * Get the game that the user plays.
     */
    public function getTeamPointsProgressAttribute()
    {
        if ($this->team_points < 1 || $this->game->max_points_team < 1) {
            return 0;
        }

        $progress = intval($this->team_points/$this->game->max_points_team*100);

        return $progress > 100 ? 100 : $progress;
    }

    /**
     * Get the game that the user plays.
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    /**
     * Get user organization games.
     */
    public function games()
    {
        return $this->hasMany('App\Game', 'organization_id', 'organization_id');
    }

    /**
     * Get user answer on question.
     */
    public function getAnswer($taskId, $qid, $private = false)
    {
        $answer = $this->hasOne('App\Answer')
            ->where('task_id', $taskId)
            ->where('question_id', $qid)
            ->where('private', $private)
            ->first();

        return $answer;
    }

    /**
     * Has user answered?
     */
    public function getTaskStatus($task)
    {
        if ($task->day === 0) {
            return 'bg-green';
        }

        $answer = $this->hasOne('App\Answer')
            ->where('task_id', $task->id)
            ->first();

        if ($answer) {
            $diff = $answer->created_at->diffInMinutes(Carbon::now());
            return $diff < 5 ? 'bg-orange' : 'bg-green';
        }

        return 'bg-red';
    }

    /**
     * Get the organization that owns the user.
     */
    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    /**
     * Get all of the user's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get all of the user's answers.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get all of the user's answers.
     */
    public function getAnswers($taskId)
    {
        return $this->hasOne('App\Answer')->where('task_id', $taskId);
    }
}
