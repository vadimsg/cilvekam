Nova.booting((Vue, router, store) => {
    Vue.component('index-text', require('./components/IndexField'))
    Vue.component('detail-text', require('./components/DetailField'))
    Vue.component('form-text', require('./components/FormField'))
})
