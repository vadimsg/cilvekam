<?php

namespace Vg\Text;

use Laravel\Nova\Fields\Field;

class Text extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'text';
}
